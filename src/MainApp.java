import project27.Model.Beans.*;
import project27.Model.View.*;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;

public class MainApp extends Application {
	
	protected int menuWidth = 900;
	protected int menuHeight = 700;
	protected int scenesWidth = 1200;
	protected int scenesHeight = 700;
	
	protected StageController stageController = new StageController();
	protected ControlsController controlsController = new ControlsController();
	protected ObservableList<Player> playersData = FXCollections.observableArrayList();
	
	public MainApp() {
		playersData = controlsController.playerDao.findAll();
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void start(Stage primaryStage) throws Exception {
	
	/****************************** Menu Scene ***************************************************/
		
		AnchorPane menuLayout = new AnchorPane();
		menuLayout.setStyle("-fx-background-image: url('menuBackground.gif');"
				  + "-fx-background-size: contain;"
				  + "-fx-background-position: center;"
				  + "-fx-background-repeat: no-repeat;");
		
			Group menuLabel = new Group();
			AnchorPane.setTopAnchor(menuLabel,(double) 0);
			AnchorPane.setLeftAnchor(menuLabel,(double) 0);
				Label menuTitle = new Label("THE BEST FREAKING\n SCOREBOARD\n EVER MADE !");
				menuTitle.setFont(new Font("Arial", 40));
				menuTitle.setTextAlignment(TextAlignment.CENTER);
				menuTitle.setAlignment(Pos.CENTER);
				menuTitle.setRotate(-45);
				
			menuLabel.getChildren().addAll(menuTitle);

			VBox menuButtons = new VBox(10);
			AnchorPane.setTopAnchor(menuButtons,(double) 20);
			AnchorPane.setRightAnchor(menuButtons,(double) 20);

				Button goScoreboard = new Button("Scoreboard");
				goScoreboard.setStyle("-fx-background-color: -fx-outer-border,-fx-inner-border,-fx-body-color;" 
					    +"-fx-background-insets: 0, 1, 2;" + "-fx-background-radius: 5, 4, 3;");  // Disable focus color & size change
				Button quit = new Button("Quit");	
				quit.setStyle("-fx-background-color: -fx-outer-border,-fx-inner-border,-fx-body-color;" 
					    +"-fx-background-insets: 0, 1, 2;" + "-fx-background-radius: 5, 4, 3;"); // Disable focus color & size change
			
			menuButtons.getChildren().addAll(goScoreboard, quit);
			
		menuLayout.getChildren().addAll(menuLabel, menuButtons);
		Scene menuScene = new Scene(menuLayout, menuWidth, menuHeight);
		
	/****************************** Primary Scene ************************************************/
	
		HBox scoreBoardLayout = new HBox();
			
			VBox leftSide = new VBox();
			leftSide.setStyle("-fx-background-color : purple;");
			leftSide.setAlignment(Pos.CENTER);
			
				TableView<Player> scores = new TableView<Player>();	
				scores.setMaxHeight(scenesHeight-200); // Top/Bottom margins for full height Tableview
				scores.setFixedCellSize(30);
				scores.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY); // Dispatch empty space between existing columns
				scores.setItems(playersData);
				scores.setPrefHeight((scores.getItems().size()) * scores.getFixedCellSize() + 25); // +25 for header
					
					TableColumn<Player, String> pseudoCol = new TableColumn<Player, String>("Pseudo");
					TableColumn<Player, Integer> winsCol = new TableColumn<Player, Integer>("Wins");
					TableColumn<Player, Integer> lossesCol = new TableColumn<Player, Integer>("Losses");
					
					pseudoCol.setStyle("-fx-alignment: CENTER;");
					winsCol.setStyle("-fx-alignment: CENTER;");
					lossesCol.setStyle("-fx-alignment: CENTER;");
					pseudoCol.setCellValueFactory(new PropertyValueFactory<Player, String>("pseudo"));
					winsCol.setCellValueFactory(new PropertyValueFactory<Player, Integer>("wins"));
					lossesCol.setCellValueFactory(new PropertyValueFactory<Player, Integer>("losses"));
				
				scores.getColumns().setAll(pseudoCol, winsCol, lossesCol);
				
			leftSide.getChildren().addAll(scores);
				
			VBox rightSide = new VBox();
			
				VBox playersInfos = new VBox(5);
				playersInfos.setStyle("-fx-background-color : orange;");
				playersInfos.setAlignment(Pos.CENTER);
			
					Label firstName = new Label();
					Label lastName = new Label();
					firstName.setFont(new Font("", 16));
					lastName.setFont(new Font("", 16));
					lastName.setPadding(new Insets(0,0,10,0));
					
					HBox alterButtons = new HBox(8);
					alterButtons.setAlignment(Pos.CENTER);
					
						Button modifyPlayer = new Button("Modify Player infos");
						Button deletePlayer = new Button("Delete Player");
						
					alterButtons.getChildren().addAll(modifyPlayer, deletePlayer);
				
					GridPane modifyPlayerInfos = new GridPane();
					modifyPlayerInfos.setAlignment(Pos.CENTER);
					modifyPlayerInfos.setHgap(10);
					modifyPlayerInfos.setVgap(3);
					modifyPlayerInfos.setPadding(new Insets(0,0,10,0));
					
						Label pseudoModifyLabel = new Label("Pseudo");
						Label firstNameModifyLabel = new Label("First Name");
						Label lastNameModifyLabel = new Label("Last Name");
						Label winsModifyLabel = new Label("Wins");
						Label lossesModifyLabel = new Label("Losses");
						
						TextField modifyPseudo = new TextField();
						TextField modifyFirstName = new TextField();
						TextField modifyLastName = new TextField();
						TextField modifyWins = new TextField();
						TextField modifyLosses = new TextField();
						
						pseudoModifyLabel.setFont(new Font("", 14));
						firstNameModifyLabel.setFont(new Font("", 14));
						lastNameModifyLabel.setFont(new Font("", 14));
						winsModifyLabel.setFont(new Font("", 14));
						lossesModifyLabel.setFont(new Font("", 14));
						
					ColumnConstraints columnRight = new ColumnConstraints();
					ColumnConstraints columnLeft = new ColumnConstraints();
						columnRight.setHalignment(HPos.RIGHT);
						columnLeft.setHalignment(HPos.LEFT);
					
					modifyPlayerInfos.getColumnConstraints().add(0, columnRight);
					modifyPlayerInfos.addRow(0, pseudoModifyLabel, modifyPseudo);
					modifyPlayerInfos.addRow(1, firstNameModifyLabel, modifyFirstName);
					modifyPlayerInfos.addRow(2, lastNameModifyLabel, modifyLastName);
					modifyPlayerInfos.addRow(3, winsModifyLabel, modifyWins);
					modifyPlayerInfos.addRow(4, lossesModifyLabel, modifyLosses);
						
					HBox alterButtons2 = new HBox(8);
					alterButtons2.setAlignment(Pos.CENTER);
						
						Button confirmModify = new Button("Confirm modification");
						Button abort = new Button("Abort");
							
					alterButtons2.getChildren().addAll(confirmModify, abort);			
				
				VBox addPlayer = new VBox(10);
				addPlayer.setStyle("-fx-background-color : brown;");
				addPlayer.setAlignment(Pos.TOP_CENTER);
					
					GridPane insertPlayerInfos = new GridPane();
					insertPlayerInfos.setAlignment(Pos.CENTER);
					insertPlayerInfos.setHgap(10);
					insertPlayerInfos.setVgap(5);
					insertPlayerInfos.setPadding(new Insets(20,0,10,0));
						
						Label addPseudo = new Label("Pseudo");
						Label addFirstName = new Label("First Name");
						Label addLastName = new Label("Last Name");
						Label addWins = new Label("Wins");
						Label addLosses = new Label("Losses");
						TextField customPseudo = new TextField();
						TextField customFirstName = new TextField();
						TextField customLastName = new TextField();
						TextField customWins = new TextField();
						TextField customLosses = new TextField();
						
						Group clearPlayerButton = new Group();
							Button clearPlayerFields = new Button("Clear Fields");
							clearPlayerFields.setStyle("-fx-background-color: -fx-outer-border,-fx-inner-border,-fx-body-color;" 
						    +"-fx-background-insets: 0, 1, 2;" + "-fx-background-radius: 5, 4, 3;"); // Disable focus color & size change
							clearPlayerFields.setPrefWidth(120);
							clearPlayerFields.setPrefHeight(40);
							clearPlayerFields.setRotate(-90);
							
						clearPlayerButton.getChildren().addAll(clearPlayerFields);
								
						insertPlayerInfos.getColumnConstraints().add(0, columnRight);
						
						addPseudo.setFont(new Font("", 14));
						addFirstName.setFont(new Font("", 14));
						addLastName.setFont(new Font("", 14));
						addWins.setFont(new Font("", 14));
						addLosses.setFont(new Font("", 14));
						addPseudo.setStyle("-fx-text-fill : white;");
						addFirstName.setStyle("-fx-text-fill : white;");
						addLastName.setStyle("-fx-text-fill : white;");
						addWins.setStyle("-fx-text-fill : white;");
						addLosses.setStyle("-fx-text-fill : white;");					
						
					insertPlayerInfos.addRow(0, addPseudo, customPseudo, clearPlayerButton);
					insertPlayerInfos.addRow(1, addFirstName, customFirstName);
					insertPlayerInfos.addRow(2, addLastName, customLastName);
					insertPlayerInfos.addRow(3, addWins, customWins);
					insertPlayerInfos.addRow(4, addLosses, customLosses);
					GridPane.setRowSpan(clearPlayerButton, GridPane.REMAINING);
						
					Button addCustomPlayer = new Button("Add Player");
					Button goBackMenu = new Button("Go back to menu");
					
				addPlayer.getChildren().addAll(insertPlayerInfos, addCustomPlayer, goBackMenu);
					
			rightSide.getChildren().addAll(playersInfos, addPlayer);
			
		scoreBoardLayout.getChildren().addAll(leftSide, rightSide);
		Scene scoreboardScene = new Scene(scoreBoardLayout, scenesWidth, scenesHeight);
		
	/****************************** Secondary Scene **********************************************/
		
		VBox secondaryLayout = new VBox();
		secondaryLayout.setStyle("-fx-background-color : beige;");
		secondaryLayout.setAlignment(Pos.CENTER);
			
			HBox comparePlayers = new HBox(30);
			comparePlayers.setAlignment(Pos.CENTER);
			
				VBox frowTVPlayerInfos = new VBox();
				frowTVPlayerInfos.setAlignment(Pos.CENTER);
				
					GridPane selectedPlayerInfos = new GridPane();
					selectedPlayerInfos.setAlignment(Pos.CENTER);
					
						Label idLabel = new Label("Id : ");
						Label pseudoLabel = new Label("Pseudo : ");
						Label firstName2Label = new Label("First Name : ");
						Label lastName2Label = new Label("Last Name : ");
						Label wins2Label = new Label("Wins : ");
						Label losses2Label = new Label("Losses : ");
						
						Label id = new Label();
						Label pseudo = new Label();
						Label firstName2 = new Label();
						Label lastName2 = new Label();
						Label wins2 = new Label();
						Label losses2 = new Label();
						
						idLabel.setFont(new Font("", 20));
						pseudoLabel.setFont(new Font("", 20));
						firstName2Label.setFont(new Font("", 20));
						lastName2Label.setFont(new Font("", 20));
						wins2Label.setFont(new Font("", 20));
						losses2Label.setFont(new Font("", 20));
						id.setFont(new Font("", 20));
						pseudo.setFont(new Font("", 20));
						firstName2.setFont(new Font("", 20));
						lastName2.setFont(new Font("", 20));
						wins2.setFont(new Font("", 20));
						losses2.setFont(new Font("", 20));
					
					selectedPlayerInfos.getColumnConstraints().add(0, columnRight);
					selectedPlayerInfos.getColumnConstraints().add(1, columnLeft);
					selectedPlayerInfos.addRow(0, idLabel, id);
					selectedPlayerInfos.addRow(1, pseudoLabel, pseudo);
					selectedPlayerInfos.addRow(2, firstName2Label, firstName2);
					selectedPlayerInfos.addRow(3, lastName2Label, lastName2);
					selectedPlayerInfos.addRow(4, wins2Label, wins2);
					selectedPlayerInfos.addRow(5, losses2Label, losses2);
					
				VBox fromFindPlayerInfos = new VBox();
				fromFindPlayerInfos.setAlignment(Pos.CENTER);
					
					VBox aimFind = new VBox(8);
					aimFind.setAlignment(Pos.CENTER);
					
						Label findTargetLabel = new Label("Id de la cible :");
						findTargetLabel.setFont(new Font("", 18));
						TextField findTarget = new TextField();
						Button launchFind = new Button("Compare");
						
					aimFind.getChildren().addAll(findTargetLabel, findTarget, launchFind);
				
					GridPane foundPlayerInfos = new GridPane();
					foundPlayerInfos.setAlignment(Pos.CENTER);
					
						Label idLabelFound = new Label("Id : ");
						Label pseudoLabelFound = new Label("Pseudo : ");
						Label firstNameLabelFound = new Label("First Name : ");
						Label lastNameLabelFound = new Label("Last Name : ");
						Label winsLabelFound = new Label("Wins : ");
						Label lossesLabelFound = new Label("Losses : ");
						
						Label idFound = new Label();
						Label pseudoFound = new Label();
						Label firstNameFound = new Label();
						Label lastNameFound = new Label();
						Label winsFound = new Label();
						Label lossesFound = new Label();
						
						idLabelFound.setFont(new Font("", 20));
						pseudoLabelFound.setFont(new Font("", 20));
						firstNameLabelFound.setFont(new Font("", 20));
						lastNameLabelFound.setFont(new Font("", 20));
						winsLabelFound.setFont(new Font("", 20));
						lossesLabelFound.setFont(new Font("", 20));
						idFound.setFont(new Font("", 20));
						pseudoFound.setFont(new Font("", 20));
						firstNameFound.setFont(new Font("", 20));
						lastNameFound.setFont(new Font("", 20));
						winsFound.setFont(new Font("", 20));
						lossesFound.setFont(new Font("", 20));
					
					foundPlayerInfos.getColumnConstraints().add(0, columnRight);
					foundPlayerInfos.getColumnConstraints().add(1, columnLeft);
					foundPlayerInfos.addRow(0, idLabelFound, idFound);
					foundPlayerInfos.addRow(1, pseudoLabelFound, pseudoFound);
					foundPlayerInfos.addRow(2, firstNameLabelFound, firstNameFound);
					foundPlayerInfos.addRow(3, lastNameLabelFound, lastNameFound);
					foundPlayerInfos.addRow(4, winsLabelFound, winsFound);
					foundPlayerInfos.addRow(5, lossesLabelFound, lossesFound);
			
			comparePlayers.getChildren().addAll(frowTVPlayerInfos, fromFindPlayerInfos);
			
			VBox secondaryButtons = new VBox(5);
			secondaryButtons.setAlignment(Pos.CENTER);
			
				Button stopCompare = new Button("Stop comparison");
				Button goPrimary = new Button("Go back");
			
			secondaryButtons.getChildren().addAll(goPrimary);		
			
        secondaryLayout.getChildren().addAll(comparePlayers, secondaryButtons);
        Scene secondaryScene = new Scene(secondaryLayout, scenesWidth, scenesHeight);
		
	/****************************** Buttons Events Handlers **************************************/
        
        quit.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) { stageController.closeStage(primaryStage); }
        });
        
	    modifyPlayer.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				controlsController.modifyPlayer(scores, playersInfos, modifyPlayerInfos, alterButtons2);
			}
	    });
	    
	    confirmModify.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				controlsController.confirmModify(scores, modifyPseudo, modifyFirstName, modifyLastName, modifyWins , modifyLosses, firstName, lastName, playersInfos, alterButtons);
			}
    	});
	    
	    abort.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				controlsController.abort(firstName, lastName, playersInfos, alterButtons);
			}
	    });
	           
        deletePlayer.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				controlsController.deletePlayer(scores);
			}
        });
        
        addCustomPlayer.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				controlsController.createPlayer(playersData, scores, customPseudo, customFirstName, customLastName, customWins, customLosses);
			}
        });
        
        clearPlayerFields.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				controlsController.clearFields(customPseudo, customFirstName, customLastName, customWins, customLosses);
			}
        });
        
        launchFind.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				controlsController.launchFind(fromFindPlayerInfos, foundPlayerInfos, secondaryButtons, stopCompare, goPrimary, findTarget, idFound, pseudoFound, firstNameFound, lastNameFound, winsFound, lossesFound);
			}
	    });
        
        stopCompare.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				controlsController.stopCompare(fromFindPlayerInfos, aimFind, secondaryButtons, goPrimary);
			}
	    });
	        
    /****************************** TextFields Events Handlers ***********************************/
        
        customWins.textProperty().addListener(new ChangeListener<String>() {
        	public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        		controlsController.onlyDecimalValueChange(customWins, oldValue, newValue);
        	}
        });
        
        customLosses.textProperty().addListener(new ChangeListener<String>() {
    		public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
    			controlsController.onlyDecimalValueChange(customLosses, oldValue, newValue);
    		}
        });
        
        modifyWins.textProperty().addListener(new ChangeListener<String>() {
	    	public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
	    		controlsController.onlyDecimalValueChange(modifyWins, oldValue, newValue);
	    	}
	    });
	    
        modifyLosses.textProperty().addListener(new ChangeListener<String>() {
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				controlsController.onlyDecimalValueChange(modifyLosses, oldValue, newValue);
			}
	    });
	        
        findTarget.textProperty().addListener(new ChangeListener<String>() {
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				controlsController.onlyDecimalValueChange(findTarget, oldValue, newValue);
			}
	    });
	
	/****************************** TableView Events Handlers ************************************/	
		
    	scores.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Player>() {
    		public void changed(ObservableValue<? extends Player> observable, Player oldValue, Player newValue) {
    			controlsController.onSingleClickTV(scores, firstName, lastName, newValue, playersInfos, alterButtons);
    		}
    	});
    	
    	scores.setRowFactory( tv -> {
        	TableRow<Player> row = new TableRow<Player>();
        	
        	row.setOnMouseClicked(event -> {
            	if (event.getClickCount() == 2) {
            		controlsController.onDoubleClickTV(row, frowTVPlayerInfos, fromFindPlayerInfos, aimFind, selectedPlayerInfos, id, pseudo, firstName2, lastName2, wins2, losses2, secondaryScene, primaryStage);
            	}
        	});
        	
        	return row ;
    	});
    	
    /****************************** Scenes switchers *********************************************/   
        
        goScoreboard.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				stageController.goScoreboard( leftSide, scores, rightSide, playersInfos, addPlayer, scoreboardScene, scenesWidth, menuWidth, scenesHeight, menuHeight, primaryStage);
			}
        });
	
		goBackMenu.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				stageController.goBackMenu(menuScene, scenesWidth, menuWidth, scenesHeight, menuHeight, playersInfos, primaryStage);
			}
		});
		
		goPrimary.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) { stageController.goBackPrimary(frowTVPlayerInfos, fromFindPlayerInfos, scoreboardScene, primaryStage); }
		});
				
    /****************************** Stage Events Handlers ****************************************/
			
		scoreboardScene.widthProperty().addListener(new ChangeListener() {
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				stageController.resizeScoreboardWidth(leftSide, scores, rightSide, primaryStage);			
			}
		});

		scoreboardScene.heightProperty().addListener(new ChangeListener() {
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				stageController.resizeScoreboarHeight(playersInfos, addPlayer, primaryStage);
			}
		});
		
		menuScene.widthProperty().addListener(new ChangeListener() {
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				stageController.resizeMenu(goScoreboard, quit, primaryStage);	
			}
		});
		
		menuScene.heightProperty().addListener(new ChangeListener() {
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				stageController.resizeMenu(goScoreboard, quit, primaryStage);	
			}
		});
		
		primaryStage.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent event) -> {
    		if (KeyCode.ESCAPE == event.getCode()) {
    			stageController.closeStage(primaryStage);
    		}
	    });	
		
	/*********************************************************************************************/	
		
		primaryStage.setScene(menuScene);
		primaryStage.setTitle("BoustiBoustiflor");
		primaryStage.show();
		
		stageController.buttonsSizeSetup(goScoreboard, quit, addCustomPlayer, goBackMenu, stopCompare, goPrimary, primaryStage);	
	}
	
	public static void main(String[] args){
		launch(args);
	}
}