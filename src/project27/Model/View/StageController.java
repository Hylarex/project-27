package project27.Model.View;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import project27.Model.Beans.Player;

public class StageController {

/****************************** Scenes switchers *********************************************/

	public void goScoreboard(VBox leftSide, TableView<Player> scores, VBox rightSide, VBox playersInfos, VBox addPlayer, Scene scoreboardScene, int scenesWidth, int menuWidth, int scenesHeight, int menuHeight, Stage primaryStage) {
		primaryStage.hide();
		primaryStage.setScene(scoreboardScene);
		primaryStage.setX(primaryStage.getX() - ((scenesWidth-menuWidth)/2));
		primaryStage.setY(primaryStage.getY() - ((scenesHeight-menuHeight)/2));
		primaryStage.show();
		
		leftSide.setMinWidth(primaryStage.getWidth()*2/3);
			scores.setMaxWidth(leftSide.getMinWidth()-50);
		rightSide.setMinWidth(primaryStage.getWidth()/3);
			playersInfos.setMinHeight(primaryStage.getHeight()/3);
			addPlayer.setMinHeight(primaryStage.getHeight()*2/3);
	}
	
	public void goBackMenu(Scene menuScene, int scenesWidth, int menuWidth, int scenesHeight, int menuHeight, VBox playersInfos, Stage primaryStage) {
		primaryStage.hide();
		primaryStage.setScene(menuScene);
		primaryStage.setX(primaryStage.getX() + ((scenesWidth-menuWidth)/2));
		primaryStage.setY(primaryStage.getY() + ((scenesHeight-menuHeight)/2));
		primaryStage.show();
		
		playersInfos.getChildren().clear();
	}
	
	public void goBackPrimary(VBox frowTVPlayerInfos, VBox fromFindPlayerInfos, Scene scoreboardScene, Stage primaryStage) {
		primaryStage.setScene(scoreboardScene);
		frowTVPlayerInfos.getChildren().clear();
		fromFindPlayerInfos.getChildren().clear();
	}
	
/****************************** Stage Events Handlers ****************************************/
	
	public void resizeScoreboardWidth(VBox leftSide, TableView<Player> scores, VBox rightSide, Stage primaryStage) {
		leftSide.setMinWidth(primaryStage.getWidth()*2/3);
		scores.setMaxWidth(leftSide.getMinWidth()-50);
		rightSide.setMinWidth(primaryStage.getWidth()/3);
	}
	
	public void resizeScoreboarHeight(VBox playersInfos, VBox addPlayer, Stage primaryStage) {
		playersInfos.setPrefHeight(primaryStage.getHeight()/3);
		addPlayer.setPrefHeight(primaryStage.getHeight()*2/3);
	}
	
	public void resizeMenu(Button goScoreboard, Button quit, Stage primaryStage) {
		goScoreboard.setPrefSize(primaryStage.getWidth()/4, primaryStage.getHeight()/10);
		quit.setPrefSize(primaryStage.getWidth()/4, primaryStage.getHeight()/10);
	}
	
	public void closeStage(Stage targetStage) { targetStage.close(); }
	
	public void buttonsSizeSetup(Button goScoreboard, Button quit, Button addCustomPlayer, Button goBackMenu, Button stopCompare, Button goPrimary, Stage primaryStage) {
		goScoreboard.setPrefSize(primaryStage.getWidth()/3.5, primaryStage.getHeight()/10);
		quit.setPrefSize(primaryStage.getWidth()/3.5, primaryStage.getHeight()/10);
		addCustomPlayer.setPrefSize(primaryStage.getWidth()/4, primaryStage.getHeight()/14);
		goBackMenu.setPrefSize(primaryStage.getWidth()/4, primaryStage.getHeight()/14);
		goPrimary.setPrefSize(primaryStage.getWidth()/5, primaryStage.getHeight()/16);
		stopCompare.setPrefSize(primaryStage.getWidth()/5, primaryStage.getHeight()/16);
	}	
} 