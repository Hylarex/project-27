package project27.Model.View;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import project27.Model.Beans.Player;
import project27.Model.DAO.PlayerDAO;

public class ControlsController {
	public PlayerDAO playerDao = new PlayerDAO();

/****************************** Buttons Events Handlers **************************************/
	
	public void clearFields(TextField customPseudo, TextField customFirstName, TextField customLastName, TextField customWins, TextField customLosses) {
		customPseudo.clear();
		customFirstName.clear();
		customLastName.clear();
		customWins.clear();
		customLosses.clear();
	}

	public void modifyPlayer(TableView<Player> scores, VBox playersInfos, GridPane modifyPlayerInfos, HBox alterButtons2) {
		playersInfos.getChildren().clear();
		playersInfos.getChildren().addAll(modifyPlayerInfos, alterButtons2);
	}
	
	public void confirmModify(TableView<Player> scores, TextField modifyPseudo, TextField modifyFirstName, TextField modifyLastName, TextField modifyWins ,TextField modifyLosses, Label firstName, Label lastName, VBox playersInfos, HBox alterButtons) {
		// Alter into maj de la TV
		
		playersInfos.getChildren().clear();
		playersInfos.getChildren().addAll(firstName, lastName, alterButtons);
	}
	
	public void abort(Label firstName, Label lastName, VBox playersInfos, HBox alterButtons) {
		playersInfos.getChildren().clear();
		playersInfos.getChildren().addAll(firstName, lastName, alterButtons);
	}
	
	public void deletePlayer(TableView<Player> scores) {
		if(scores.getItems().size() != 0) {
			playerDao.delete(scores.getSelectionModel().getSelectedItem());
			scores.getItems().remove(scores.getSelectionModel().getSelectedItem());
			scores.setPrefHeight((scores.getItems().size()) * scores.getFixedCellSize() + 25);
		}
	}
	
	public void createPlayer(ObservableList<Player> playersData, TableView<Player> scores, TextField customPseudo, TextField customFirstName, TextField customLastName, TextField customWins, TextField customLosses) {
		if (!(customPseudo.getText().isEmpty()||customFirstName.getText().isEmpty()||customLastName.getText().isEmpty()||customWins.getText().isEmpty()||customLosses.getText().isEmpty())) {
			Player newPlayer = new Player();
			newPlayer.setPseudo(customPseudo.getText());
			newPlayer.setFirstName(customFirstName.getText());
			newPlayer.setLastName(customLastName.getText());
			newPlayer.setWins(Integer.parseInt(customWins.getText()));
			newPlayer.setLosses(Integer.parseInt(customLosses.getText()));
			
			playerDao.create(newPlayer);
			playersData.add(newPlayer);
			
			scores.setPrefHeight((scores.getItems().size()) * scores.getFixedCellSize() + 25);
			
			clearFields(customPseudo, customFirstName, customLastName, customWins, customLosses);
		}	
	}
	
	public void launchFind(VBox fromFindPlayerInfos, GridPane foundPlayerInfos, VBox secondaryButtons, Button stopCompare, Button goPrimary, TextField findTarget, Label idFound, Label pseudoFound, Label firstNameFound, Label lastNameFound, Label winsFound, Label lossesFound) {
		if(!findTarget.getText().isEmpty()) {
			Player foundPlayer = playerDao.find(Long.parseLong(findTarget.getText()));
			
			idFound.setText("" + foundPlayer.id);
			pseudoFound.setText(foundPlayer.pseudo);
			firstNameFound.setText(foundPlayer.firstName);
			lastNameFound.setText(foundPlayer.lastName);
			winsFound.setText("" + foundPlayer.wins);
			lossesFound.setText("" + foundPlayer.losses);
			
			ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList(
					new PieChart.Data("Wins", foundPlayer.wins),
					new PieChart.Data("Losses", foundPlayer.losses));
				
		    final PieChart winsLossesChart = new PieChart(pieChartData);
			
			fromFindPlayerInfos.getChildren().clear();
			secondaryButtons.getChildren().clear();
			fromFindPlayerInfos.getChildren().addAll(foundPlayerInfos, winsLossesChart);
			secondaryButtons.getChildren().addAll(stopCompare, goPrimary);	
			findTarget.clear();
		}
	}
	
	public void stopCompare(VBox fromFindPlayerInfos, VBox aimFind, VBox secondaryButtons, Button goPrimary) {
		fromFindPlayerInfos.getChildren().clear();
		secondaryButtons.getChildren().clear();
		fromFindPlayerInfos.getChildren().addAll(aimFind);
		secondaryButtons.getChildren().addAll(goPrimary);	
	}
/****************************** TextFields Events Handlers ***********************************/

	public void onlyDecimalValueChange(TextField target, String oldValue, String newValue) {
		if (!newValue.matches("\\d{0,5}?")) {
			target.setText(oldValue);
		}
	}
	
/****************************** TableView Events Handlers ************************************/
	
	public void onSingleClickTV(TableView<Player> scores, Label firstName, Label lastName, Player newValue, VBox playersInfos, HBox alterButtons) {
		if(scores.getItems().size() != 0) {
			firstName.setText("First Name : " + newValue.firstName);
			lastName.setText("Last Name : " + newValue.lastName);
			
			playersInfos.getChildren().clear();
			playersInfos.getChildren().addAll(firstName, lastName, alterButtons);
		}
	}

	public void onDoubleClickTV(TableRow<Player> row, VBox frowTVPlayerInfos, VBox fromFindPlayerInfos, VBox aimFind, GridPane selectedPlayerInfos, Label id, Label pseudo, Label firstName2, Label lastName2, Label wins2, Label losses2, Scene secondaryScene, Stage primaryStage) {
		Player rowPlayerData = row.getItem();
		
		id.setText("" + rowPlayerData.id);
		pseudo.setText(rowPlayerData.pseudo);
		firstName2.setText(rowPlayerData.firstName);
		lastName2.setText(rowPlayerData.lastName);
		wins2.setText("" + rowPlayerData.wins);
		losses2.setText("" + rowPlayerData.losses);
		
		ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList(
			new PieChart.Data("Wins", rowPlayerData.wins),
			new PieChart.Data("Losses", rowPlayerData.losses));
		
        final PieChart winsLossesChart = new PieChart(pieChartData);
		
		frowTVPlayerInfos.getChildren().addAll(selectedPlayerInfos, winsLossesChart);
		fromFindPlayerInfos.getChildren().addAll(aimFind);
        
		primaryStage.setScene(secondaryScene);
	}	
}
