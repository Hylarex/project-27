package project27.Model.DAO;

import java.sql.*;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import project27.Model.Beans.Player;

public class PlayerDAO extends DAO<Player> {

	public Player find(long id) {
		Player player = new Player();
	
		try {
			Statement stmt = connect.createStatement();
			ResultSet rs = stmt.executeQuery("Select * from player where id = " + id);
		
			while (rs.next()) {
				int bdd_id = rs.getInt("id");
				String bdd_pseudo = rs.getString("Pseudo");
				String bdd_firstName = rs.getString("First_Name");
				String bdd_lastName = rs.getString("Last_Name");
				int bdd_wins = rs.getInt("Wins");
				int bdd_loses = rs.getInt("Losses");
			
				player.setId(bdd_id);
				player.setPseudo(bdd_pseudo);
				player.setFirstName(bdd_firstName);
				player.setLastName(bdd_lastName);
				player.setWins(bdd_wins);
				player.setLosses(bdd_loses);
			}
			
			return player;
		}
		catch (Exception e) {
			System.out.println("EtudiantDAO: find() FAILED : " + e.getLocalizedMessage());
		}
			return null;
	}
	
	public ObservableList<Player> findAll() {
		ObservableList<Player> playersData = FXCollections.observableArrayList();
	
		try {
			Statement stmt = connect.createStatement();
			ResultSet rs = stmt.executeQuery("Select * from player");
		
			while (rs.next()) {
				Player currentPlayer = new Player();
				
				int bdd_id = rs.getInt("id");
				String bdd_pseudo = rs.getString("Pseudo");
				String bdd_firstName = rs.getString("First_Name");
				String bdd_lastName = rs.getString("Last_Name");
				int bdd_wins = rs.getInt("Wins");
				int bdd_loses = rs.getInt("Losses");
			
				currentPlayer.setId(bdd_id);
				currentPlayer.setPseudo(bdd_pseudo);
				currentPlayer.setFirstName(bdd_firstName);
				currentPlayer.setLastName(bdd_lastName);
				currentPlayer.setWins(bdd_wins);
				currentPlayer.setLosses(bdd_loses);
				
				playersData.add(currentPlayer);
			}
			
			return playersData;
		}
		catch (Exception e) {
			System.out.println("EtudiantDAO: findAll() FAILED : " + e.getLocalizedMessage());
		}
			return null;
	}

	public Player create(Player newPlayer) {
		try {		
			Statement stmt = connect.createStatement();
	        
			@SuppressWarnings("unused")
			Integer useless = stmt.executeUpdate("insert into player (pseudo, first_name, last_name, wins, losses) values"
				+ "('" + newPlayer.pseudo + "','"
				+ newPlayer.firstName + "','"
				+ newPlayer.lastName + "','"
				+ newPlayer.wins + "','"
				+ newPlayer.losses + "');", Statement.RETURN_GENERATED_KEYS);
			
			ResultSet rs = stmt.getGeneratedKeys();
			
	        if (rs.next()){
	        	newPlayer.id = rs.getInt(1);
	        }
		
			return newPlayer;	
		}
		catch (Exception e) {
			System.out.println("EtudiantDAO: create() FAILED : " + e.getLocalizedMessage());
		}
		
		return null;
	}
	
	public Player update(Player obj) {
	return null;
	}
	
	public void delete(Player target) {
		try {
			Statement stmt = connect.createStatement();
			@SuppressWarnings("unused")
			int rs = stmt.executeUpdate("Delete from player where id = " + target.id);
		}
		catch (Exception e) {
			System.out.println("EtudiantDAO: delete() FAILED : " + e.getLocalizedMessage());
		}	
	}
}








	
	
	