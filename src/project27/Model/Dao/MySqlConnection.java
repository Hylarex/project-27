package project27.Model.DAO;

import java.sql.*;

public class MySqlConnection {
    
    private static String url = "jdbc:mysql://localhost/scoreboard?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static String user = "scoreboard_user";
    private static String password = "boustiflor";
    
    private static Connection connect;
  
    public static Connection getInstance() {
        if(connect == null){
        	try {
            connect = DriverManager.getConnection(url, user, password);
        	} 
        	catch (SQLException e) {
        		e.printStackTrace();
        	}
    }  
    return connect;
    }
}