package project27.Model.Beans;

public class Player {
	
	public int id;
	public String pseudo;
	public String firstName;
	public String lastName;
	public Integer wins;
	public Integer losses;
	
	/************** Getters & Setters **************/
	
	public int getId() { return id; }
	public void setId(int id) { this.id = id; }
	
	public String getPseudo() { return pseudo; }
	public void setPseudo(String pseudo) { this.pseudo = pseudo; }
	
	public String getFirstName() { return firstName; }
	public void setFirstName(String firstName) { this.firstName = firstName; }
	
	public String getLastName() { return lastName; }
	public void setLastName(String lastName) { this.lastName = lastName; }
	
	public int getWins() { return wins; }
	public void setWins(Integer wins) { this.wins = wins; }
	
	public int getLosses() { return losses; }
	public void setLosses(Integer losses) { this.losses = losses; }
	
	/***********************************************/
	
	public void display() {
		System.out.println("Joueur : " + id + " / "+ firstName + " " + lastName + " / " + wins + " - " + losses);
	}
}

